module.exports = {
  siteTitle: "Platform B - Samen Werkt ", // <title>
  shortSiteTitle: "Platform B - official Site", // <title> ending for posts and pages
  siteDescription: "Platform B is a foundation that is aimed to improve intercultural and interrelegious dialogs",
  siteUrl: "http://www.platfomb.be",
  // pathPrefix: "",
  siteImage: "preview.jpg",
  siteLanguage: "en",

  /* author */
  authorName: "Platform B",
  authorTwitterAccount: "@platformB",

  /* info */
  headerTitle: "Platform B",
  headerSubTitle: "Informatie over ons",

  /* manifest.json */
  manifestName: "Platform B",
  manifestShortName: "PlatformB", // max 12 characters
  manifestStartUrl: "/index.html",
  manifestBackgroundColor: "white",
  manifestThemeColor: "#666",
  manifestDisplay: "standalone",

  // gravatar
  // Use your Gravatar image. If empty then will use src/images/jpg/avatar.jpg
  // Replace your email adress with md5-code.
  // Example https://www.gravatar.com/avatar/g.strainovic@gmail.com ->
  // gravatarImgMd5: "https://www.gravatar.com/avatar/1db853e4df386e8f699e4b35505dd8c6",
  gravatarImgMd5: "",

  // social
  authorSocialLinks: [
   {
      name: "facebook",
      url: "https://www.facebook.com/Platform-B-797690133920523/"
    }
  ]
};
