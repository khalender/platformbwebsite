---
title: Platform B
---

**Platform B** is: een organisatie die in --België-- elke dag meer en meer contacten legt met vertegenwoordigers van andere levensbeschouwingen, culturen en gemeenschappen. Onze werking beoogt door gesprek en ontmoeting elkaar beter te leren kennen en gelijklopende waarden en inzet te ontdekken. Het aanvaarden van verschillen en misschien tegenstellingen in elkaars cultuur en levensbeschouwing is noodzakelijk om stappen naar elkaar toe te zetten. Platform B wil iedereen, ongeacht zijn overtuiging, zijn ras of cultuur, daadwerkelijk met elkaar in contact brengen om elkaars afkomst en eigenheid beter te leren kennen en wederzijds te respecteren. We streven ernaar dat mensen uit verschillende levensbeschouwingen en culturen elkaar meer zouden durven te benaderen om samen aan een leefbaardere maatschappij in België te bouwen.
