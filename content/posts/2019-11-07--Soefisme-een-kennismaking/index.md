---
title: Soefisme; een kennismaking
category: "Activiteiten"
cover: photo-001-5454dfd56.jpg
author: Belgie
---

![photo](./photo-001-5454dfd55.jpg)

## Soefisme; een kennismaking ##


7 november 2019
C-Mine Genk
Vandaag heeft de boekvoorstelling van “Soefisme; een kennismaking” plaatsgevonden in C-Mine te Genk. Het was een boeiende uiteenzetting van de vertaler Marc Colpaert. ‘Vroeger had het soefisme geen naam, maar was het een werkelijkheid’. Nu heeft het een naam, maar je moet zoeken naar de werkelijkheid achter de naam. Die zoektocht maakt William Chittick in dit boek’, vertelde Marc. In een maatschappij waarin de grote religies nog steeds tegenover elkaar staan en waarin het wantrouwen naar de islam voor veel maatschappelijke onrust zorgt, is de centrale boodschap van het soefisme, waarin harmonie, schoonheid en liefde een centrale rol vervullen, uitermate actueel. ‘Het was een monnikenwerk, maar tegelijk een verademing om dit boek te vertalen. Ook een soort eerherstel aan de islam’ aldus Colpaert die zelf cultuurfilosoof is. 
Voor de aanwezigen was dit een unieke kans om meer inzicht te krijgen op de spirituele en humane kant van de Islam. Inhoudelijk ging het vooral over de essentie van het soefisme maar ook vergelijkingen tussen de islamitische en christelijke spiritualiteit en mystiek. In zijn uiteenzetting stelde Colpaert ook dat soefisme een middel kan zijn tegen de huidige maatschappelijke problemen zoals polarisatie, extremisme en radicalisme.
De avond werd georganiseerd door Platform B en geleid door onze voorzitter Bahattin Koçak.

Enkele vragen die deze avond aan bod kwamen:
1.	Waarom de nood aan dit boek? 
2.	Wat is soefisme (in een notendopje)? Hoe zou je soefisme uitleggen aan ‘dummies’? Wat is de achtergrond van het soefisme? Wat is de essentie van het soefisme?
3.	Is er een verband tussen religie, klassieke mystiek en volksmystiek?
4.	Wat is de relatie tussen het soefisme en de Islam? Is er bvb een verschil tussen de islam die een soefie belijdt en de islam die een gewone moslim belijdt?
5.	Zou je het soefisme als een aparte strekking naast de Islam zetten?
6.	Kan je soefisme vergelijken met iets uit het christendom?
7.	Hoe wordt je een soefie? Zijn er, indien ja welke, voorwaarden om een soefie te worden? Wie kan/mag zich een soefie noemen?
8.	Kan een niet-moslim ook het soefisme belijden? 
9.	Wat is de plaats van het soefisme of de filosofie achter het soefisme binnen de globale islam? Zijn er veel volgers of is het eerder een beperkt (elite)groepje?
10.	Soefisme; is dat nog iets van vandaag? Is er een toekomst (en eventueel een rol weggelegd) voor het soefisme vandaag de dag?
11.	Kan soefisme een middel zijn tegen de huidige maatschappelijke problemen zoals polarisatie, extremisme, radicalisme etc?
12.	Soefisme wordt vaak voorgesteld als iets dat open staat voor dialoog en een tolerantere versie van de Islam. Is de Islam zelf dat dan niet?
13.	Soefisme wordt ook sterk geassocieerd met het humanisme omdat het op de eerste plaats de mens centraal stelt. Het wordt ook gezien als de meest humanistische kant van de Islam. Klopt dit?
14.	Bij soefisme denken we hoofdzakelijk aan Roemi, de wereldwijd bekende dichter/poëet en de wervelende derwisjen? Klopt dit? Zijn er andere grote namen?


![photo](./photo-001-5454dfd56.jpg)
![photo](./photo-001-5454dfd57.jpg)
![photo](./photo-001-5454dfd58.jpg)
![photo](./photo-001-5454dfd60.jpg)



