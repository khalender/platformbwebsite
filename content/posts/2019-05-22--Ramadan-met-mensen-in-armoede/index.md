---
title: Ramadan met mensen in armoede
category: "Activiteiten"
cover: photo-8728379-389283092.jpg
author: Sint-Truiden
---

![photo](./photo-2938-38921380832.jpg)
## Platform B viert Ramadan met mensen in armoede
De eerste iftaravond van Platform B was meteen een schot in de roos. I.s.m. armoedevereniging vzw OnderOns hebben we een Ramadanmaaltijd/iftaravond georganiseerd in de basisschool Momentum in Sint-Truiden. Voor vele aanwezigen was het een eerste keer in hun leven.
Grote dank aan alle medewerkers, vrijwilligers, Nathalie en Liza van OnderOns, Kris Selis- directeur van de basisschool die ons in alle vertrouwen, zonder kosten haar school ter beschikking stelde. Uiteraard ook een grote dank aan burgemeester Veerle Heeren voor haar aanwezigheid, samen met schepen Jurgen Reniers. Het was een topavond! Voor herhaling vatbaar... Stay tuned!

Artikel in Het Belang van Limburg
[https://www.hbvl.be/cnt/dmf20190524_04421511/moslims-vieren-ramadan-met-mensen-in-armoede](https://www.hbvl.be/cnt/dmf20190524_04421511/moslims-vieren-ramadan-met-mensen-in-armoede)


![photo](./photo-2382138389-2783923.jpg)
![photo](./photo-323989-9827328.jpg)
![photo](./photo-87312837-827382.jpg)
![photo](./photo-837293820-82398273.jpg)
![photo](./photo-82738-827383728.jpg)
















