---
title: Congregaties en broederschappen in het Christendom
category: "Activiteiten"
cover: photo-3123123213-32312312.jpg
author: Bibliotheek Genk
---
![photo](./photo-3123123213-32312312.jpg)
Vanuit de vraag om elkaar beter te leren kennen werd Hans Geybels, docent aan de Faculteit Theologie en Religiewetenschappen Leuven en medeoprichter van de christelijke denktank LOGIA, gevraagd voor een uiteenzetting over de broderschappen en congregaties in het Christendom. Een boeiende avond met veel aanwezigen, veel vragen en vooral de wil om stappen te zetten in een betere en sterkere samenleving.



![photo](./photo-312dsdds-32312312.jpg)

