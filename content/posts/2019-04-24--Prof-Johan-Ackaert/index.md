---
title: “De limieten van de pacificatiepolitiek en de politieke geschiedenis van België”
category: "Activiteiten"
cover: photo-5486567893457-26512931.jpg
author: UHasselt
---

![photo](./photo-556557893457-67644932.jpg)

Woensdag 24 april 2019 was prof. Johan Ackaert, decaan van de Faculteit rechten van de UHasselt onze gastspreker. In aanloop van de naderende verkiezingen bracht hij meer inhoud en duiding over de politieke geschiedenis en de verzuiling van België. Een interessant topic omdat we zoveel kleuren zien in de politiek en toch het achterliggend idee niet direct merken. 
Politiek gaat niet alleen over partijen en het parlement, maar ook over drukkingsgroepen, sociale en economische factoren. Prof. Ackaert bracht in zijn uiteenzetting enerzijds een historisch perspectief hierover maar had het ook over de (actuele) spanningen en conflicten zoals:

* Levensbeschouwelijke conflicten
* Sociaal-economische conflicten
* Communautaire conflicten

![photo](./photo-54545457-455435656.jpg)
![photo](./photo-5454893457-245342932.jpg)
![photo](./photo-5487893457-98792932.jpg)
![photo](./photo-556557893457-67644932.jpg)
![photo](./photo-4323857893457-7675612932.jpg)
![photo](./photo-54832437893457-25452932.jpg)







