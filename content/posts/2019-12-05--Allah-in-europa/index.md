---
title: Allah in Europa
category: "Activiteiten"
cover: image-001-92319.jpg
author: Belgie
---

![photo](./image-001-92319.jpg)

## Allah in Europa ##


‘Allah in Europa’. Dat was wat ons op een donderdagavond samenbracht met Jan Leyers, de maker van de gelijknamige documentaire over moslims in Europa. Tien jaar na 'De weg naar Mekka' verkende Jan Leyers de Islam in Europa. 

Groeit er zoiets als en Europese versie van de islam? Is dat ook wat moslims zelf willen? En valt het alom heersende wantrouwen jegens de islam te overwinnen? Op zoek naar antwoorden trok Jan Leyers van Sarajevo naar Brussel. Hij ging met Bosnische moefti's op bedevaart, maakte een moslimkamp in de Hongaarse poesta mee, werd wegwijs gemaakt in een Parijse banlieue en ging in Londen met de voorzitter van een shariaraad op pad. Jan Leyers luisterde naar traditionele gelovigen en nieuwe bekeerlingen, naar liberalen en fundamentalisten, naar de stem van de kenner en de stem van de straat. Met de oren van een seculiere westerling die op zijn zeventiende rotsvast geloofde dat de toekomst van Europa er één zou zijn zonder religie en vandaag verbaasd aanziet hoe het opperwezen terrein herovert. 

'Allah in Europa' is het relaas van een tocht door een wereld die zich niet in cijfers of statistieken laat vatten. Een meeslepend avontuur dat ontroert en ontreddert, onthutst en hoop inboezemt. 

Voor de leden van Platform B was vooral de focus op moslims in België het uitgangspunt. De diversiteit in de moslimgemeenschap, de verschillende opvattingen, de moeilijkheden om een moslim(a) te zijn in de Belgische samenleving en nog vele andere zaken…  De eerste avond met Jan Leyers was te kort om alles te bespreken. 
Volgende afspraak met Jan in het voorjaar van 2020.



![photo](./image-001-92320.jpg)
![photo](./image-001-92321.jpg)



