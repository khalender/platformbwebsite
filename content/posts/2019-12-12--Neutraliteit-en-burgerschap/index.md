---
title: Neutraliteit en burgerschap in een multiculturele/multireligieuze maatschappij
category: "Activiteiten"
cover: image-001-92325.jpg
author: Belgie
---

![photo](./image-001-92325.jpg)


**"Neutraliteit en burgerschap in een multiculturele/multireligieuze maatschappij"** was het thema in de Bibliotheek van Genk met als gastspreker professor Rik Torfs. 

Torfs sprak voor volle zaal. Het was full house in de bibliotheek.

Rik Torfs begon met een uitleg over zijn vier punten betreffende neutraliteit en burgerschap. Van de grenzen van vrije meningsuiting tot het tasten van de mensenrechten wanneer het gaat over religie... Wat is godsdienstvrijheid en wat zijn de juridische grenzen? Bestaat neutraliteit echt? Het hoe en wat van religieus en actief pluralisme... 

Is godsdienst een bedreiging voor de mensenrechten? Indien ja, wat dan met de mensenrechten want die garanderen juist het recht op geloof... Belangrijke subthema's die zorgden voor een diepgaand gesprek in het tweede gedeelte van de avond. 



![photo](./image-001-92322.jpg)
![photo](./image-001-92323.jpg)
![photo](./image-001-92324.jpg)
![photo](./image-001-92326.jpg)
![photo](./image-001-92327.jpg)



