---
title: Iftar in de Japanse Tuin van Hasselt
category: "Activiteiten"
cover: photo-0001-0001.jpg
author: Japanse Tuin Hasselt
---


<style>

@import url('https://fonts.googleapis.com/css?family=Abril+Fatface&display=swap');
body { 
  background-color: #F2F5F6;
 }

/* center the blockquote in the page */
.blockquote-wrapper {
   display: flex;
   height: 100vh;
   padding: 0 20px;
}

/* Blockquote main style */
.blockquote {
    position: relative;
    font-family: 'Barlow Condensed', sans-serif;
    max-width: 620px;
    margin: 20px auto;
    align-self: center;
}

/* Blockquote header */
.blockquote h1 {
    font-family: 'Abril Fatface', cursive;
    position: relative; /* for pseudos */
    color: #3A4042;
    font-size: 1.4rem;
    font-weight: normal;
    line-height: 1.4;
    margin: 0;
    border: 2px solid #F2F5F6;
    border: solid 2px;
    border-radius:20px;
    padding: 18px;
}

/* Blockquote right double quotes */
.blockquote h1:after {
    content:"";
    position: absolute;
    border: 2px solid #3A4042;
    border-radius: 0 50px 0 0;
    width: 60px;
    height: 60px;
    bottom: -60px;
    left: 50px;
    border-bottom: none;
    border-left: none;
    z-index: 3; 
}

.blockquote h1:before {
    content:"";
    position: absolute;
    width: 80px;
    border: 6px solid #F2F5F6;
    bottom: -3px;
    left: 50px;
    z-index: 2;
}

/* Blockquote subheader */
.blockquote h4 {
    position: relative;
    color: black;
    font-size: 1.3rem;
    font-weight: 400;
    line-height: 1.2;
    margin: 0;
    padding-top: 15px;
    z-index: 1;
    margin-left:150px;
    padding-left:12px;
    font-size: 20px;
}

 
.blockquote h4:first-letter {
  margin-left:-12px;
}
</style>

![photo](./photo-0001-0001.jpg)


<div class="blockquote-wrapper">
  <div class="blockquote">
    <h1>
     ‘De beste manier om te vertellen wie je bent, is zijn wat je wilt vertellen’.<br>
     「あなたが誰であるかを知るための最善の方法は、あなたが伝えたいものになることです。」<br>
     `Anata ga daredearu ka o shiru tame no saizen no hōhō wa, anata ga tsutaetai mono ni naru kotodesu.' 
     </h1>
  </div>
</div>

Die kans hebben de leden van Platform B benut tijdens een Ramadandiner op een unieke plaats, in de Japanse Tuin van Hasselt.   
In deze, soms donkere dagen, wordt het samenleven tussen volkeren, culturen, godsdienstige gemeenschappen en beschavingen zwaar op de proef gesteld. 
Elkaar bemoedigen en te sterken op de weg van de vrede is daarom een lichtpunt en van grote waarde.   
Leren samen leven is een uitdaging van formaat. De leden en sympathisanten van Platform B durven deze uitdaging aangaan door met iedereen, ongeacht afkomst, geloof, cultuur, ras en geaardheid te praten, hen te ontmoeten, te erkennen en samen hand in hand te bouwen aan de toekomst.   
Want dankzij dialoog kunnen we streven naar een vreedzame samenleving met respect voor elkaars verschillen.   
Ramadan is de maand bij uitstek voor een ontmoeting, kennismaking, in dialoog gaan maar ook genieten van lekker eten,...  Al deze ingrediënten waren vandaag aanwezig op de iftar van Platform B op een zeer unieke plaats, in de Japanse Tuin van Hasselt. 
Academici, vertegenwoordigers van diverse levensbeschouwingen, leden van socio-culturele verenigingen maar vooral, allemaal vrienden waren aanwezig.   
Op het einde van de avond overhandigde Jac De Bruyn namens de christelijke gemeenschap een brief met een boodschap ter gelegenheid van het einde van de Ramadan.    
![photo](./photo-0001-0002.jpg)

<div class="blockquote-wrapper">
  <div class="blockquote">
    <h1>
    ご参加いただきありがとうございます。 一緒に我々はそれを美しい夜にした。 それは一緒に働くので... <br><br>
Go sanka itadaki arigatōgozaimasu. Issho ni wareware wa sore o utsukushī yoru ni shita. Sore wa issho ni hataraku node...  
     </h1>
  </div>
</div>

  


Grote dank aan alle aanwezigen. Samen hebben we er een mooie avond van gemaakt. Want samen werkt…  



![photo](./photo-0001-0003.jpg)
![photo](./photo-0001-0004.jpg)
![photo](./photo-0001-0005.jpg)
![photo](./photo-0001-0006.jpg)
![photo](./photo-0001-0007.jpg)
![photo](./photo-0001-0008.jpg)
![photo](./photo-0001-0009.jpg)
![photo](./photo-0001-0010.jpg)
![photo](./photo-0001-0011.jpg)
![photo](./photo-0001-0012.jpg)
![photo](./photo-0001-0013.jpg)







