---
title: FranciscanCall4Peace
category: "Activiteiten"
cover: photo-0001-114445.jpg
author: Belgie
---

![photo](./photo-0001-114445.jpg)

## Ook Platform B steunt de vredesactie ‘FranciscanCall4Peace ##

**800 jaar geleden deed Franciscus van Assisi het ondenkbare. In augustus 1219 trok hij in volle kruistochtentijd naar het kruisvaarderskamp in Damiate (Egypte) met de wens de sultan van Egypte te ontmoeten, de gedoodverfde vijand van het christendom. De paus had het westen massaal gemobiliseerd om ten strijde te trekken tegen de moslims en het Heilig Land te heroveren. Als enkeling probeerde Franciscus deze spiraal van geweld te doorbreken. Wars van het heersende vijanddenken, trok hij de sultan van Egypte tegemoet met een geheel eigen vredesmissie...**


Enerzijds is er Francis, een ruiter in de provinciale Assisi, met een sterrenkijker, gevangengenomen als krijgsgevangene en een jaar gevangengezet, daarna vrijgelaten, mishandeld, slachtoffer van geweld.

Anderzijds een vergeten moslimprins, de jonge neef van de briljante Saladin, de beroemde tegenstander van Richard Leeuwenhart, die is opgegroeid in het paleis van de sultan en is klaargestoomd voor de troon in een opleiding die doordrenkt is van islamitisch leren.

Deze ontmoeting werpt licht op de oorsprong van de kruistochten van dehumaniserende retoriek tegenover niet-Europeanen en niet-christenen en hoe deze taal gebruikmaakte van iets primitiefs en gevaarlijks en gewelddadigs in de menselijke geest. 

Niet alleen spreekt het direct tot enkele van de Oost-West-conflicten van vandaag, het verhaal zelf is zeer dramatisch met vele fascinerende wendingen en bochten, en met centrale karakters die boeiender zijn dan zelfs hun legendes beweren.

De oorlog leek geen einde te hebben. Maar toen  ondernam Francis van Assisi één van de dapperste risico's in de geschiedenis. Hij zette een stap in de richting van vredestichting door vijandige linies te overschrijden om de sultan te ontmoeten, de leider van een zogenaamd wrede en satanische vijand.

De sultan reageerde met één van de grootste humanitaire daden in de geschiedenis van de oorlogsvoering door de gehate kruisvaarders te redden van de honger toen de overstroming van de Nijl 50.000 soldaten had omringd. 

De ontmoeting tussen deze twee moedige mannen heeft het gif uit het conflict weggezogen en uiteindelijk deze schijnbaar eindeloze oorlog beëindigd. 

Deze ontmoeting  inspireert ook oplossingen voor de negatieve sfeer waarin we ons vandaag bevinden.
De actie FranciscanCaal4peace  laat ons ook afvragen waarom ontmoetingen zoals deze vaak vergeten zijn en zelfs onbestudeerd, terwijl de details van de oorlog tot in de nauwe puntjes wel geweten zijn.  Waarom ligt de focus van de geschiedenis niet op de inspanningen van deze twee gelovige mannen? Want dit kan een fundament worden van verbondenheid, saamhorigheid en vrede. 

Ontmoeting tussen mensen van oprecht geloof en overtuiging is een essentiële taak voor mensen van goede wil. Vredestichting is een weerspiegeling van het karakter van God. De sultan en de heilige Franciscus tonen ons de mogelijkheid van Gods bredere doeleinden om verschillende soorten mensen een weg naar vrede en wederzijds respect te helpen vinden, een les die essentieel is voor christenen en moslims.

Op 4 oktober 2019 om 14u, op het moment van het vrijdagsgebed van de moslims, zullen over heel het land de kathedrale klokken luiden. Een symbolische vredesactie om de ontmoeting van 800 jaar geleden extra in de kijker te zetten. 
Platform B nam ook deel aan een aantal ontmoetingen op deze dag.


<div style="font-style: italic; padding-left:20px; backgound-color: #;">

### Brief van de Franciscanen ter gelegenheid van ‘FranciscanCall4Peace’ ### 
​“Dat op iedere gebedstijd en wanneer de klokken luiden heel het volk over heel de aarde steeds lof en dank brengt aan de almachtige God” Franciscus van Assisi (1219)

Beste moslimvrienden,

Salaam! Vrede en alle goeds! 
Binnen de Franciscaanse gemeenschap vieren wij dit jaar dat de Heilige Franciscus van Assisi 800 jaar geleden sultan Malek Al-Kamil van Egypte ontmoette. Franciscus trok naar het Midden-Oosten in 1219 en in de Egyptische havenstad Damietta kwam het tot deze bijzondere historische ontmoeting. We ervaren dit nog steeds als een sterk voorbeeld van dialoog.
Twee vreemden ontmoetten elkaar in vrede, met één adem en hartslag voor God. De een was en bleef christen, de ander was en bleef moslim. De ontmoeting heeft een bijzonder effect gehad, in het bijzonder op Franciscus. Hij had geloofsverwantschap gevoeld met een moslim en was onder de indruk door de oproep tot gebed die klonk doorheen stad en streek. Kort daarop schreef hij de tekst hierboven in een brief aan de stadsbestuurders en de religieuze verantwoordelijken van zijn orde.
In het voetspoor van Franciscus willen wij op vrijdag 4 oktober 2019 wereldwijd de klokken laten luiden tijdens het islamitische vrijdaggebed. We herdenken op deze hoogdag het heengaan van Franciscus van Assisi. We wensen zo Franciscus’ verlangen om samen God te loven wereldwijd waar te maken. Wij willen ons die dag biddend bij jullie, onze moslimbroeders & zusters, aansluiten. 
Van Franciscus is ook het Loflied op de Allerhoogste. Een christelijk gebed dat in zijn vormgeving duidelijk geïnspireerd is door en verwant is met de 99 schone Namen van Allah. In een gezamenlijk verlangen naar broederlijkheid, verbondenheid en vrede zou het symbolisch erg krachtig zijn als die vrijdag in de moskeeën de 99 schone Namen van Allah gereciteerd zouden worden, en in de kerken het Loflied van Franciscus. We zullen hiervoor een gebeds/bezinningskaart uitbrengen. 
Een website www.FranciscanCall4Peace.org zal dit initiatief ondersteunen en in kaart brengen over heel de wereld. Dit initiatief zal vanuit ‘TAU – Franciscaanse Spiritualiteit Vandaag’ en ‘KaVIS – Kapucijnen Vlaanderen Internationale Solidariteit’ georganiseerd en gecoördineerd worden.
Wij hopen dat wij mogen rekenen op uw broederlijke deelname aan dit initiatief en dat wij zo samen op vrijdag 4 oktober 2019 om 14u. over heel de wereld een hoopvol teken kunnen geven van gebed, vrede en verzoening onder christenen en moslims en alle mensen van goede wil.
Hopend onze vraag ter harte te willen nemen,
groeten wij u.

Meer info? Neem contact op via info@franciscancall4peace.org
<div>