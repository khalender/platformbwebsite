---
title: Dag van de Ashoera
category: "Activiteiten"
cover: photo-0001-24343.jpg
author: Antwerpen
---


![photo](./photo-0001-24346.jpg)

## Dag van de Ashoera ##

Op de 10e dag van de maand Moeharram, de eerste maand van de islamitische kalender, vieren moslims de Dag van Ashoera. 
Interessant om te weten is dat dit gebaseerd is op het Joodse Jom Kipoer of de Grote Verzoendag.
Op deze dag is het belangrijk dat Joodse mensen vasten. Ook moslims vasten vaak op deze dag, alhoewel dit niet verplicht is. 

**De Antwerpse vereniging vzw Inspiration organiseerde een boottocht met Ashoera als onderwerp. Na het ontbijt mocht de voorzitter van Platform B een korte toespraak houden over Ashoera en de symboliek hieromtrent;**<br><br>
**Enkele passages:**

_“Op die dag van de maankalender hebben volgens islamitische bronnen tien belangrijke gebeurtenissen plaatsgevonden._ _De meest bekende gebeurtenis op deze dag en in kader van de Ashoera, is de redding van de profeet Noach, vzmh, samen met zijn volgelingen._
_Volgens één van de vertellingen reisden zij met de ark tijdens de zondvloed. Op de laatste dag werd al het voedsel dat was overgebleven op de ark verzameld en daar werd een heerlijke pudding van gemaakt. Dit heet de ashoerapudding. Deze pudding wordt door veel moslims gedeeld met buren en vrienden ter herdenking van al deze gebeurtenissen._

_Kortom, op de tiende dag van de islamitische jaartelling herdenken moslims bovenstaande gebeurtenissen. In het bijzonder wordt lering getrokken uit het optreden van Noach. Deze profeet, dat beschouwd kan worden als de tweede vader van de mensheid, staat bekend om zijn verdraagzaamheid, geduld en overgave. Tijdens de laatste dagen van de zondvloed kampte de overlevenden met een tekort aan voedsel, aan boord van de Ark. Hij riep op om alle eten restjes bij elkaar te brengen waaruit de maaltijd of beter bekend "de pudding van Ashura" werd bereid. Ter nagedachtenis van deze dag bereiden moslims dan ook de pudding van Ashura tot op de dag van vandaag voor”._<br><br>
… <br>
Persoonlijk vind ik dat juist in deze tijd van polarisatie, waarbij de discussies in onze samenleving gaan over polarisatie, radicalisering, terrorisme, oorlogen en vluchtelingen, een verzoenende boodschap zoals de dag van Ashoera hard nodig is.
Het delen van voedsel en samen eten versterkt het contact tussen mensen ongeacht ras, geloof of ideologische achtergrond. Het biedt tevens gelegenheid om universele waarden zoals wederzijds begrip en respect te bevorderen.
 
_“In deze, soms donkere dagen waarin onze wereld al jaren verkeert, wordt het samenleven tussen volkeren, culturen, godsdienstige gemeenschappen en beschavingen zwaar op de proef gesteld. Daarom is een ontmoeting als deze, waarin mensen met verschillende achtergronden elkaar willen ontmoeten om elkaar te bemoedigen en te sterken op de weg van de vrede, voor mij een lichtpunt en van grote waarde. Vandaag door de Ashoera, morgen Kerstmis of Pasen; een andere keer het lentefeest of De Ramadan en zo zijn er genoeg dagen en redenen om samen te komen”._<br><br> 
… 


_**“De Ashoera staat symbool voor vele boodschappen.** Ik ga proberen om in een notendopje enkele van deze boodschappen weer te geven._
_Een eerste boodschap klinkt misschien wat cliché maar is de meest gekende, nl. dat we met zijn allen op dezelfde boot zitten à (dit is onze wereld/land/maatschappij)._
_Het is onze taak als mens om ervoor zorgen dat deze veilig en wel aanmeert. Deze boodschap staat symbool voor een gedeelde verantwoordelijkheid naar onze kinderen toe._
_We zijn misschien niet verantwoordelijk voor het verleden maar we kunnen wel onze verantwoordelijkheid opnemen voor de toekomst._
_Iedereen heeft een verantwoordelijkheid op deze boot. Niemand heeft het recht om het boot te doen zinken. Deze verantwoordelijkheid komt op de eerste plaats niet vanuit het geloof maar vooral omdat we mensen zijn. Dit is onze grootste gemeenschappelijke deler, maar ook het belangrijkste feit en waarheid om verbondenheid te creëerden”._

…
![photo](./photo-0001-24344.jpg)

“Als het gaat over **verantwoordelijkheid**, denk ik meteen aan een quote van **Fethullah Gülen**, een hedendaagse Islamitische denker. Hij zegt het volgende:

 _**“Er is een nood aan een soort verantwoordelijkheidsethiek, een soort planetaire verantwoordelijkheid; een verantwoordelijkheid voor de wereld met ons, rondom ons en na ons. We zijn allemaal verantwoordelijk voor onze omgeving, onze familie en ONZE wereld”. (einde citaat)”**_

…


“In de Ashoerapudding zitten allerlei bestanddelen die afzonderlijk belangrijk zijn maar veel voorstellen net zoals de aanwezige culturen die verschillen met elkaar maar veel kunnen betekenen voor onze samenleving. Een belangrijk voorbeeld is waarschijnlijk het effect van Andaloesië en de moslims op Europa in de Middeleeuwen. Door de kleine bestanddelen met elkaar te combineren ontstaat er iets dat bruikbaar is voor een lekkere smaak, net zoals de verrijking van diversiteit en dialoog voor een sterke en leefbare multiculturele samenleving”.

…


_**“Een lekkere Ashoera maken is een beproeving  op zich.**_ Net zoals samen leven ook een grote uitdaging is voor ons allen in deze tijden.
De kunst van het samenleven bestaat erin om, ondanks alles wat er vandaag gebeurt op micro en macroniveau, voor een harmonie te zorgen.

We kunnen de wereld niet op ons eentje redden maar we kunnen wel enerzijds onze wil voor het goede tonen en anderzijds, in de mate van het mogelijke, een bijdrage leveren voor een harmonische samenleving”. 

![photo](./photo-0001-24345.jpg)