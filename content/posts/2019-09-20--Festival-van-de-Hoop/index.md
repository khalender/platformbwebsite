---
title: Festival van de Hoop
category: "Activiteiten"
cover: photo-0001-24343.jpg
author: Hasselt
---

![photo](./photo-0001-24343.jpg)


## Festival van de Hoop ##

Op 20 september 2019 organiseerde de pedagogische begeleidingsdienst van het Bisdom van Hasselt een Festival van de Hoop. De voorzitter van Platform B werd gevraagd om een korte lezing te geven over hoop. De titel van zijn bijdrage luidde ‘Inshallah’ wat zo goed als “Als Allah het wil’ betekent.
Een samenvatting van zijn lezing: <br>


_**“Is er in deze tijden van crisissen nood aan hoop?
Is geloof, islam of andere, een bron van hoop? Welke elementen in de inhoud en de beleving van mijn overtuiging bevatten aanzetten tot een ‘hoopvol leven’?”**_

…

**“De centrale vraag is: ben ik op de eerste plaats mens of moslim?**
<br>

Dé uitdaging was en is nog steeds om een goede mens en moslim te worden maar ook zorgen voor een leefbare omgeving voor iedereen; moslims, christenen, humanisten en alle anderen.  Ondanks alle moeilijkheden en tegenslagen; denk maar aan 9/11, het alledaagse racisme en discriminatie, de economische crisis, landen die failliet gaan, de groeiende armoede, terrorisme, verzuring, onbegrip;… 
De vraag is dan natuurlijk hoe je nog hoopvol kan kijken naar de toekomst, als er toch zoveel slechte dingen gebeuren?
Maar juist daar, op momenten van weemoed en wanhoop, kwam voor mij altijd het geloof ter hulp. 
Mijn geloof is mijn bron en hoop is één van de aspecten dat mijn mens-zijn en geloof ook versterkt. Het verruimt mijn visie en het geeft mij een positieve kijk voor de toekomst. 
…

Allah zegt in de Koran het volgende:
<br>

**_“… wanhoopt niet aan de barmhartigheid van Allah…”,_**

Deze woorden herhalen zich verschillende keren in de Koran; 
Toen Yusuf/Jozef in een put werd achtergelaten door zijn broers was het Allah die hem geruststelde met deze woorden.
Toen Mozes met de farao moest praten, toen Ibrahim op de brandstapel stond, waren het dezelfde woorden die hen geruststelde.
Zelfs toen Maria beviel van Jezus, zonder dat een man haar had aangeraakt en ze niet wist wat te antwoorden aan de priesters, was het Allah die haar geruststelde met dezelfde woorden.
Vandaag zegt Allah hetzelfde:

**_“… wanhoopt niet aan de barmhartigheid van Allah…”,_**
<br>

**_Met andere woorden; heb hoop!”_**
…

**“HOPEN BETEKENT VERTROUWEN HEBBEN IN ALLAH!**
  
Maar ook in zijn schepselen, de mens. Want wij zijn het die hem vertegenwoordigen op aarde. Daarom dat investeren in de mens o zo belangrijk is.
  
Iemand die hoopt en dit ook uitstraalt, is alsof hij een soort licht bezit en dit ook doorgeeft aan anderen. Want hoop is een fakkel, hoop is licht,
  
Hoop doet leven! Wil je leven en laten leven, blijf dan hopen. 
Hoop laat een mens geloven in een goed einde, hoop laat een mens daadwerkelijk werken om doelen te bereiken. 
  
Zonder hoop zou de mens nooit ontwikkeld zijn. Het is de drijfveer geweest voor alle ontwikkelingen en bewegingen die in de gehele geschiedenis van de wereld hebben plaatsgevonden.
  
Voor alle duidelijkheid, hoop is niet alleen voor gelovigen. Hoop is in elk mens, elk individu aanwezig.”
…
  
**“INSHALLAH-**  
   
_In de islam bestaan er een aantal uitdrukkingen om de bewondering en de dankbaarheid voor de schepper te uiten. Subhan’allah of maash’allah **wanneer je iets moois, iets waardevols ziet, elhamdulillah als dankwoord in alles wat je meemaakt** en zo ook insha’Allah **om je vertrouwen in Hem te verwoorden.**_ 
  
_**Insha’Allah** betekent 'bij Gods wil' of ‘met de wil van Allah’. De betekenis is vergelijkbaar met het christelijke Deo volente._
  
_Het is een Arabisch woordje maar wordt door bijna alle moslims gebruikt. Het wordt vooral uitgesproken bij een toekomstverwachting, bijvoorbeeld een afspraak. Insha’Allah benadrukt voor mij mijn geloof in God/Allah._ 
  
_Het laat mij geloven in een goed einde._  
_Insha’Allah geeft hoop, zowel aan degene die het zegt als aan degene die het hoort._
_In deze, soms donkere dagen waarin onze wereld al jaren verkeert, wordt het samenleven tussen volkeren, culturen, godsdienstige gemeenschappen en beschavingen zwaar op de proef gesteld.”_








