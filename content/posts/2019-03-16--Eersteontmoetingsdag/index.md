---
title: Eerste ontmoetingsdag tussen levensbeschouwingen in Limburg
category: "Activiteiten"
cover: photo-548743857893457-203712932.jpg
author: Kerk van Oud-Waterschei, Genk
---

![KerkvanOud-Waterschei](./photo-548743857893457-203712932.jpg)

Op 16 maart 2019 nam Platform B deel aan de eerste ontmoetingsdag tussen de verschillende levensbeschouwingen in Limburg. 
De avond begon met een muzikale inleiding, ingevuld met mystieke melodieën uit diverse hoeken. Daarna werd er samen gekeken naar de film ‘The Sultan and the Saint’. Deze film werd gemaakt naar aanleiding van de ontmoeting tussen Franciscus van Assise en de toenmalige Sultan van Egypte. Hieronder kan je een korte reflectie terugvinden over deze film.

"The Sultan and The Saint" vertelt één van de belangrijkste, maar helaas ook verloren verhalen uit de geschiedenis. 
800 jaar geleden deed Franciscus het ondenkbare. In augustus 1219 trok hij in volle kruistochtentijd naar het kruisvaarderskamp in Damiate (Egypte) met de wens de sultan van Egypte te ontmoeten, de gedoodverfde vijand van het christendom. De paus had het westen massaal gemobiliseerd om ten strijde te trekken tegen de moslims en het Heilig Land te heroveren. Als enkeling probeerde Franciscus deze spiraal van geweld te doorbreken. Wars van het heersende vijanddenken, trok hij de sultan van Egypte tegemoet met een geheel eigen vredesmissie...

"The Sultan and The Saint is het is van Franciscus van Assisi en de Sultan van Egypte Kamil el-Malik, en hun ontmoeting op een bloederig slagveld.
Twee mannen van geloof, één rondtrekkende christelijke prediker, de andere de heerser van een moslimrijk. 
Enerzijds is er Francis, een ruiter in de provinciale Assisi, met een sterrenkijker, gevangengenomen als krijgsgevangene en een jaar gevangengezet, daarna vrijgelaten, mishandeld, slachtoffer van geweld.

Anderzijds een vergeten moslimprins, de jonge neef van de briljante Saladin, de beroemde tegenstander van Richard Leeuwenhart, die is opgegroeid in het paleis van de sultan en is klaargestoomd voor de troon in een opleiding die doordrenkt is van islamitisch leren.
De film werpt licht op de oorsprong van de kruistochten van dehumaniserende retoriek tegenover niet-Europeanen en niet-christenen en hoe deze taal gebruikmaakte van iets primitiefs en gevaarlijks en gewelddadigs in de menselijke geest. 

Niet alleen spreekt het direct tot enkele van de Oost-West-conflicten van vandaag, het verhaal zelf is zeer dramatisch met vele fascinerende wendingen en bochten, en met centrale karakters die boeiender zijn dan zelfs hun legendes beweren.

![photo](./photo-548743857656457-203712932.jpg)

De oorlog leek geen einde te hebben. Maar toen  ondernam Francis van Assisi één van de dapperste risico's in de geschiedenis. Hij zette een stap in de richting van vredestichting door vijandige linies te overschrijden om de sultan te ontmoeten, de leider van een zogenaamd wrede en satanische vijand.
De sultan reageerde met één van de grootste humanitaire daden in de geschiedenis van de oorlogsvoering door de gehate kruisvaarders te redden van de honger toen de overstroming van de Nijl 50.000 soldaten had omringd. 

De ontmoeting tussen deze twee moedige mannen heeft het gif uit het conflict weggezogen en uiteindelijk deze schijnbaar eindeloze oorlog beëindigd. 
Het verhaal zelf is niet alleen heel dramatisch met vele fascinerende wendingen, met centrale personages die belangrijker zijn dan zelfs hun legendes ze zouden hebben, maar het inspireert oplossingen voor de negatieve sfeer waarin we ons vandaag bevinden.

Deze film laat ons ook afvragen waarom ontmoetingen zoals deze vaak vergeten zijn en zelfs onbestudeerd, terwijl de details van de oorlog tot in de nauwe puntjes wel geweten zijn.  Waarom ligt de focus van de geschiedenis niet op de inspanningen van deze twee gelovige mannen? Want dit kan een fundament worden van verbondenheid, saamhorigheid en vrede. 

Ontmoeting tussen mensen van oprecht geloof en overtuiging is een essentiële taak voor mensen van goede wil. Vredestichting is een weerspiegeling van het karakter van God. De sultan en de heilige Franciscus tonen ons de mogelijkheid van Gods bredere doeleinden om verschillende soorten mensen een weg naar vrede en wederzijds respect te helpen vinden, een les die essentieel is voor christenen en moslims.

Na de film was er ruimte voor ontmoeting tussen de aanwezigen tijdens de pauze. 
Na de pauze was het tijd voor een sema-ceremonie. Na een korte inleiding over het soefisme trad een derwisj/semazen op en gaf een korte voorstelling van de dans van de wervelende derwisjen.

Soefisme is de religie van de liefde, zeggen soefie’s, zij die het soefisme beleven.
Soefisme is in de eerste plaats een islamitische methode om tot innerlijke vervolmaking en evenwicht te komen en een bron van intense en geleidelijk toenemende overgave. Het is geen sekte of rechtsschool binnen de islam. Ook geen vernieuwing of stroming die afwijkt van het canonieke geloof. 
Het is vooral de vastberaden weg van een groep mensen die, bij wijze van spreken, hunkeren naar de liefde van God. 
![photo](./photo-548743857893457-76756767.jpg)



