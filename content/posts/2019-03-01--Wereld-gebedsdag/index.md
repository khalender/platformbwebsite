---
title: Wereldgebedsdag 2019
category: "Activiteiten"
cover: photo-dsdsddsd.jpg
author: De Blauwe Bassin-Hasselt
---


![1 maart 2019](./photo-dsdsddsd.jpg)


Op 1 maart 2019 namen we deel aan de Wereldgebedsdag in Hasselt. Dit jaar was het thema 'Verbondenheid'. Ook Platform B heeft vanuit een islamitische visie een boodschap gebracht over dit thema.

Iedere eerste vrijdag van maart gaat het gebed de wereld rond. 
In vele landen komen mensen samen op deze vrijdag. In België gebeurt dat ook op vele plaatsen. Zo ook in Limburg.

Wereldgebedsdag brengt mensen van verschillende overtuigingen samen.

![1 maart 2019](./photo-sdksdlksd-werldgebeidsdag.jpg)
![1 maart 2019](./photo-djaslkdjlask-wereldgebeidsdah.jpg)




