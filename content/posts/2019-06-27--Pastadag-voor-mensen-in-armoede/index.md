---
title: Pastadag voor mensen in armoede
category: "Activiteiten"
cover: photo-0001-11775.jpg
author: Sint-Truiden
---




De leden van Platform B blijven  niet stil. Voor de maandelijkse ontbijtsessie werd er vandaag een koude pasta verzorgd voor de vrijwilligers en leden van Onder Ons, een vzw dat zich inzet voor mensen en gezinnen in armoede en eenzamen. 
Platform B heeft als missie dialoog, wederzijds respect, tolerantie, begrip, vrede en samenwerking tussen mensen met verschillende achtergronden te stimuleren. Zowel civiele initiatieven als doeltreffende beleidsmaatregelen zijn noodzakelijk om het samen-leven in onze steeds diversere maatschappij te garanderen. De vrijwilligers van Platform B zijn bezield door een geest van dienstbaarheid.
Na de succesvolle iftaravond in mei was dit alweer een prachtig initiatief waarbij dialoog en sociale cohesie de belangrijkste ingrediënten waren. 




![photo](./photo-0001-1155.jpg)
![photo](./photo-0001-11121.jpg)
![photo](./photo-0001-11144.jpg)
![photo](./photo-0001-11199.jpg)
![photo](./photo-0001-11775.jpg)
![photo](./photo-0001-11985.jpg)
![photo](./photo-0001-11987.jpg)







