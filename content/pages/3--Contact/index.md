---
title: Contact
---
**Email ons:** [info@platformb.be](emailto:info@platform.be)

**Volg ons op sociale media**
<!-- [![Facebook](./facebook.png)](https://www.facebook.com/Platform-B-797690133920523)
<a href="https://www.facebook.com/Platform-B-797690133920523">
[facebook](./facebook.png)
</a> -->

<style>
@import url('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');

a, a:hover {
	text-decoration: none;
    color: #fff;
}

.socialbtns, .socialbtns ul, .socialbtns li {
	margin: 0;
	padding: 5px;
    font-size: 30px;
    color: #fff;
}

.socialbtns li {
    list-style: none outside none;
    display: inline-block;
    color: #fff;
}

.socialbtns .fa {
    color: #fff;
	background-color: #FF686B;
	width: 60px;
    height: 60px;
    padding-top: 16px;
	border-radius: 60px;
	-moz-border-radius: 60px;
	-webkit-border-radius: 60px;
	-o-border-radius: 60px;
}

.socialbtns .fa:hover {
	color: #fff;
	background-color: #FFA69E;
}
</style>

<br>
<div align="center" class="socialbtns">
<ul>
<li><a href="https://www.facebook.com/Platform-B-797690133920523" target=”_blank” class="fa fa-lg fa-facebook" style="color: #fff;text-decoration: none;"></a></li>
<!-- <li><a href="#" class="fa fa-lg fa-twitter"></a></li>
<li><a href="#" class="fa fa-lg fa-google-plus"></a></li>
<li><a href="#" class="fa fa-lg fa-github"></a></li>
<li><a href="#" class="fa fa-lg fa-pinterest"></a></li>
<li><a href="#" class="fa fa-lg fa-linkedin"></a></li>
<li><a href="#" class="fa fa-lg fa-instagram"></a></li>
<li><a href="#" class="fa fa-lg fa-youtube"></a></li> -->
</ul>
</div>

