---
title: Platform B
---



### Platform B:
De bekende Duitse filosoof Habermas zegt: “Respect voor iedereen betekent niet dat je enkel je kennissen respecteert, maar ook de andere, omdat hij anders is”.
Iedereen aanvaarden zoals hij is, zonder onderscheid van taal, ras en geslacht en je medemens respecteren omwille van zijn mens-zijn, is de grondregel van Platform B. Ook al is de kleur, de overtuiging of de cultuur van de “andere” anders, toch zijn we allemaal mensen…
Laten we niet vergeten dat we allen op hetzelfde schip leven.

Universele waarden zoals liefde, respect, hoffelijkheid en begrip bevorderen, haat en geweld met liefde overwinnen, geen enkel mens voor een ander opofferen, harmonie tussen mensen scheppen en bevorderen zijn de basisdoelstellingen van Platform B.

Platform B is:
een organisatie die in België elke dag meer en meer contacten legt met vertegenwoordigers van andere levensbeschouwingen, culturen en gemeenschappen. Onze werking beoogt door gesprek en ontmoeting elkaar beter te leren kennen en gelijklopende waarden en inzet te ontdekken. Het aanvaarden van verschillen en misschien tegenstellingen in elkaars cultuur en levensbeschouwing is noodzakelijk om stappen naar elkaar toe te zetten.
Platform B wil iedereen, ongeacht zijn overtuiging, zijn ras of cultuur, daadwerkelijk met elkaar in contact brengen om elkaars afkomst en eigenheid beter te leren kennen en wederzijds te respecteren. We streven ernaar dat mensen uit verschillende levensbeschouwingen en culturen elkaar meer zouden durven te benaderen om samen aan een leefbaardere maatschappij in België te bouwen.

Platform B is een vrijwilligersorganisatie, berust op de schouders van actieve moslims van eigen bodem, die bewust zijn van het belang van de dialoog. Bij Platform B staat niet alleen de interreligieuze dialoog centraal, maar enerzijds ook de intra-dialoog en anderzijsds de interculturele dialoog.
Onze werking beoogt, door gesprek en ontmoeting, elkaar beter te leren kennen en gelijklopende waarden en inzet te ontdekken. Het aanvaarden van verschillen en misschien tegenstellingen in elkaars cultuur en godsdienst is noodzakelijk om stappen naar elkaar toe te zetten.

Platform B streeft ernaar om in het wereldwijd proces van toenadering, vanuit een islamitisch perspectief, een sterke partner te zijn en de boodschap van dialoog en hoffelijkheid aan alle mensen te verkondigen.

### Missie:

Al meer dan 50 jaar komen moslims en niet-moslims elkaar overal tegen in deze maatschappij. Zowel op straat als in de winkels, fabrieken, ziekenhuizen, en noem maar op…  Onherroepelijk ontmoeten we elkaar overal. De ontmoeting is er, maar welke vorm deze ontmoeting heeft speelt een belangrijke rol. Deze kan zowel een afwijzing zijn als een kritiek of afsluiting. Als het zo is, dan leven we volkomen langs elkaar heen. Deze vorm van ontmoeting kan een bom zijn voor onze maatschappij. Want zulk een ontmoeting betekent dat we elkaar niet willen kennen. Aangezien onbekendheid onbemindheid met zich meebrengt, is het aan ons,  om na te denken wat deze onbemindheid met zich kan meebrengen.

Andersom geloven we er sterk in dat het wegwerken van de onbemindheid een belangrijke aanleiding kan zijn voor het verdwijnen van racisme, onrechtvaardigheid en al de negatieve zaken waarmee de mensheid vandaag geconfronteerd wordt.

Een verkenning en erkenning op alle niveaus van ONZE samenleving…. dat  is waar we naar toe streven.

 

Het ligt in de aard van de mens nieuwsgierig te zijn en te zoeken naar waarheid. Deze fundamentele aandrift kan een voedingsbodem zijn voor dialoog. Het diepste verlangen van de mens is wellicht zijn kinderen, die deze bejaarde wereld nog tot het einde der tijden zullen aandoen, iets van essentieel en fundamenteel belang na te laten; gedreven door een dieper verlangen de eigen vergankelijkheid in eeuwigheid te veranderen.

Genegenheid, deugd, barmhartigheid, rechtvaardigheid en eendracht zijn universele onderdelen van geestelijke verlichting en geluk die nimmer hun waarde zullen verliezen. Ze zijn de voorwaarden voor een betekenisvol en begenadigd leven en vormen het beste nalatenschap die wij zouden kunnen wensen.

Dit is de gedachte vanuit welke Platform B tracht bij te dragen aan de verwezenlijking van een samenleving waar vrede en de duurzame waarden en principes die onmisbaar zijn voor handhaving blijvend worden gekoesterd.

 

 In de praktijk hebben we volgende activiteiten op de agenda staan:

 

* Interreligieuze ontmoetingen
* Lezingen, conferenties, symposia over actuele themata
* Jaarlijkse Ramadanmaaltijden in verschillende steden en gemeenten
* Ontmoetingen-kennismakingen met religieuze leiders, culturen,  academica, politici…
* Interreligieuze gebedsavonden, gebedswakes
* Actief deelnemen in lokale overlegplatforms en werkgroepjes
* Interculturele reizen organiseren
* Culturele evenementen zoals “De Wervelende Derwisjen”,
* Deelname aan lokale evenementen
* Ontwikkelen van diverse folders
* Themadagen in samenwerking met maatschappelijke organisaties, religieuze groeperingen en andere organisaties

## Visie

“Dialoog is onvermijdbaar”

De 20e eeuw heeft enorme veranderingen in alle levensgebieden gezien. Miljoenen mensen zijn uit de armoede getrokken dankzij de beschikbaarheid en ontginning van enorme voorraden van natuurlijke hulpmiddelen. Wetenschappelijke vooruitgang en industrialisatie werd hierdoor mogelijk gemaakt. Nochtans was er een prijs voor deze ontwikkeling. Een hoge prijs. Deze eeuw heeft politieke en economische liberalisatie gezien. Wereldrijken vielen uit elkaar. Nooit is er zoveel oorlogsgeweld geweest. Nieuwe ideologieën werden met verschrikkelijke gevolgen toegepast. Politieke en sociale  instellingen werden weggehoond met toenemend cynisme en pessimisme tot gevolg. Vooruitgang verving stilstand en meer en meer mensen zoeken controle over hun eigen leven en bestemming. Mensen proberen ook meer beroep te doen op “elkaar”.  In deze zin is dialoog onvermijdbaar geworden. Dialoog op alle vlakken, ook op levensbeschouwelijk vlak. Wat er in de geschiedenis is gebeurd en welke fouten er ook zijn gemaakt, helaas zijn wij niet in staat om ze te veranderen.  Dus heeft het voor ons ook geen zin om dit in discussie te brengen. Waar we wel sterk in geloven, is het feit dat we wel een les kunnen trekken uit de fouten van vroeger. Een positieve les kan een licht voor de toekomst betekenen. Zelfkritiek is hierbij een voorname ‘must’. In plaats van vooroordelen jegens anderen, zou het beter zijn om eerst met zelfkritiek te beginnen…”

 

Geslacht, cultuur, religie, taal, huidskleur zijn enkele voorbeelden waarin individuen en gemeenschappen verschillen tonen. Deze verschillen worden vandaag helaas misbruikt en aangeduid als redenen voor samenlevingsproblemen. De diversiteit in geslacht, cultuur, religie, taal en huidskleur wordt geassocieerd met maatschappelijke problemen en vooral het samenleven. Als gevolg ontstaat polarisatie tussen de verschillende groepen die allemaal deel uitmaken van dezelfde maatschappij. Daar waar deze elementen voor verbondenheid kunnen zorgen, worden ze jammer genoeg misbruikt als wapens om onze samenleving en de wil tot samenleven te ondermijnen.

In tegenstelling tot de beweringen dat de toenemende diversiteit zou leiden tot samenlevingsproblemen zijn wij als moslims ervan overtuigd dat diversiteit een verrijking is voor onze maatschappij.

Platform B wil een partner zijn in het versterken van deze maatschappij.

 

Religie; of levensbeschouwing… Een materie waarvan de plaats en erkenning nog steeds bijzonder actueel is. Het lijkt een problematiek te zijn in onze huidige samenleving; maar het gaat verder dan dat… De vraag van vandaag is welke rol religie of levensbeschouwing heeft in het streven naar een  maatschappelijk model met als uitgangspunt het “samenleven”. Welk is de plaats van religie/levensbeschouwing in een maatschappij waar democratie, rechtvaardigheid, verdraagzaamheid, gelijkheid en vrijheid de basiselementen van vormen?

 

Platform B is ontstaan uit de nood om deze vraag te beantwoorden vanuit een islamitisch perspectief. Platform B streeft er naar om in het wereldwijd proces van wederzijdse benadering, de erkenning en uitdrukking van religieuze en levensbeschouwelijke overtuigingen in onze democratische maatschappij op een positieve manier te weergeven. Niet enkel islam, maar alle religies en levensovertuigingen komen in dit proces aan bod.

“Coming together is a beginning, keeping together is a progress, working together is a succes”- Henry Ford